# Implementação {#sec:implementation}

Para o cumprimento dos requisitos levantados em secções anteriores foi
escolhida uma multiplicidade de tecnologias em cada uma das componentes
aplicacionais. Este diagrama deixa clara a arquitetura da aplicação, quer em
componentes, quer em tecnologias.

![Diagrama de arquitetura final](figures/diagrams/arquitetura.png){ #fig:architecture }

Em termos de interações podemos constatar que a _WebApp_ mantém apenas
comunicação com o _Backend_ (_REST API_), através mensagens **HTTP**. O
_Backend_, por sua vez, necessita de consultar a base de dados aplicacional
(mantida em _MongoDB_) para responder às interrogações do _Frontend_ e
mantê-la atualizada, no caso, de as mensagens recebidas implicarem alterações
na mesma. Quando, ao Domingo, é necessário calendarizar as tarefas que já
foram criadas o _Backend_ contacta o _Delegator_ (por **HTTP**) para que este
execute esta tarefa. O _Backend_ também mantém atualizada uma base de dados
de _Logs_ em _MySQL_ que posteriormente é consultada pelo _BackOffice_ para
produzir análises estatísticas do uso da aplicação.

## Tecnologias adotadas no _Frontend_ {#sec:front_tech}

O _Frontend_ consiste numa _Single Page Aplication_ com suporte a uma
utilização _offline_. A transição entre páginas não exige pedidos de páginas
novas ao servidor uma vez que o _routing_ é feito do lado do cliente,
especificamente, no _browser_ do utilizador. Esta característica fornece uma
experiência de navegação praticamente instantânea, com transições suaves e
elegantes para o utilizador, utilizando pequenas animações quando se fazem
pedidos à API em algumas das iterações.

![Página de inicial](figures/website/land.png){ #fig:land }

![Página de _Log in_](figures/website/login.png){ #fig:login }

![Página de _Sign up_](figures/website/singup.png){ #fig:signup }

![Página das tarefas semanais](figures/website/schedule.png){ #fig:schedule }

![Página dos _Workspaces_](figures/website/workspaces.png){ #fig:workspaces }

![Página de _Workspace_](figures/website/workspace.png){ #fig:workspace }

![Página de administração de _Workspace_](figures/website/manage.png){ #fig:manage }

![Menu inicial](figures/website/menu.png){ #fig:menu }

![Página de perfil e de utilizador](figures/website/profile.png){ #fig:profile }

![Menu de criação de tarefa](figures/website/createtask.png){ #fig:create }

![Menu de atualização de perfil](figures/website/updateprofile.png){ #fig:update }

![Página de recuperação das credenciais de acesso](figures/website/forgot.png){ #fig:forgot }

![Página de resposta bem sucedida](figures/website/sentresetmobile.png){ height=355px }
![Página de resposta mal sucedida](figures/website/failedresetmobile.png){ height=355px }
![Página de recuperação de palavra-passe](figures/website/recover_mobile.png){ height=355px }

\begin{figure}[!h]
\caption{Páginas de sucesso e insucesso e recuperação de palavra-passe}
\label{fig:status}
\end{figure}

![Página de erro 404](figures/website/404.png){ #fig:404 }

A WebApp é totalmente responsiva e adapta-se aos ecrãs de multiplas
dimensões, tais como, _smartphones_, _tabletes_, portáteis e _desktops_. A
aplicação foi desenvolvida usando a _framework_ _GatsbyJS_ que se baseia na
biblioteca _ReactJS_ para o desenvolvimento de interfaces de utilizador. A
linguagem de programação, por isso, utilizada é o _JavaScript_ que é 100%
suportado em todos os _browsers_ atuais. _ReactJS_ fornece uma excelente
experiência de desenvolvimento, com acesso a muitos recursos e funcionalidade
de aumentam a produtividade do desenvolvedor e resulta num produto rápido e
interativo de forma fluída.

O design implementado segue as linhas orientadoras do _Ant Design_,
baseando-se nos componentes fornecidos também pela sua biblioteca.

As iterações com a API utilizam _Axios_, uma biblioteca de _JavaScript_
que funciona tanto no _browser_ como no servidor. Esta característica poderá
ser útil para maior facilidade de transição para _Server Side Rendering_, por
exemplo.

## Tecnologias adotadas no _Backend_ {#sec:back_tech}

Escolhemos a linguagem de programação _Java_ para este componente
aplicacional porque considerarmos que esta é uma linguagem robusta e,
principalmente, por ser aquela com que a equipa tem mais experiência e
conhecimento técnico. Uma outra razão que suporta esta escolha é facto de
existir uma _framework_ que se chama **Spring** que facilita as interações
com a base de dados e, principalmente, fornece uma maneira criar aplicações
web de forma muito facilitada, no nosso caso, uma **API REST**. Escolhemos
_MongoDB_ para suportar a base de dados da aplicação por ser uma base de
dados documental que torna bastante fácil o armazenamento da informação se
considerarmos cada objecto _User_ e _Workspace_ do modelo _Java_ como um
documento. A _framework_ mencionada também fornece um excelente suporte,
reduzindo imensamente a dificuldade da implementação da _REST API_ que dá
suporte ao _Frontend_.

Durante a interação com o _Frontend_ tornou-se rapidamente evidente que as
imagens de perfil, quer dos utilizadores quer dos _workspaces_, não poderiam
ser guardados na base de dados idealizada (por limitações no tamanho dos
campos por parte do _MongoDB_). Assim, optamos por guardar as imagens num
_host_ externo (**ImgBB**, constituíndo isto uma das interações com _API's_
remotas).

Aquando da recuperação de contas por parte de utilizadores que se tenham
esquecido das suas credenciais de acesso é necessário enviar um email com um
_token_ de validação de identidade para que possa aceder a uma interface de
recuperação de conta. Para realizar esta ação utilizamos uma _API_ remota
chamada **Mailgun** que fornece um meio para enviar um email a destino
arbitrário permitindo definir o corpo da mensagem e destinatário através de
um simples pedido autenticado à _API_.

Apesar de estarmos cientes que uma base de dados em _MongoDB_ não necessita
de _schema_ rígido associado optamos por manter uma certa rigidez na forma
adotada na base de dados. Na base de dados que suporta o funcionamento da
aplicação optamos por manter duas coleções: `Users`e `Workspaces`, que
conforme se percebe pela estrutura de dados que cada um dos objetos constituí
que o bastante para fornecer suporte à aplicação.

![Base de dados _MongoDB_](figures/diagrams/db_schema.png){ #fig:db }

Para fornecer suporte de documentação à _API_ disponibilizada pelo nosso
serviço de _Backend_ recorremos à ferramenta **Swagger UI** que integramos
com a ferramenta de modo a atualizar a documentação de cada vez que a
aplicação (_Backend_) é corrida. Da maneira que implementamos a documentação
cada vez que um _endpoint_ for adicionado ou removido é desnecessário ter a
preocupação de atualizar a documentação, uma vez que tal será automaticamente
feito uma vez que a aplicação recomece. O serviço adotado para a documentação
supre na totalidade as necessidades que foram surgindo ao longo do
desenvolvimento do projeto, visto que: disponibiliza uma lista dos
_endpoints_ suportados, que _Path Variables_ devem ser fornecidas e o qual o
_Body_ esperado quando são feitos pedidos dirigidos a esses mesmos
_endpoints_. A documentação referida pode ser visitada em
<https://delegatewise-backend.herokuapp.com/docs>.

Por forma a atribuição alguma organização ao componente do _Backend_ optamos
por implementar por implementar 3 controladores (classes _Java_ que alojam
métodos que lidam com pedidos dirigidos a _endpoints_ específicos) principais
que são: **MainController**, **UsersController** e **WorkspacesController**.
O critério utilizado para a sua separação foi essencialmente as coleções da
base de dados a que os métodos precisam de aceder para que conseguissem, com
sucesso, produzir o meu resposta ao pedido. Deste modo, em **MainController**
estão alojados _endpoints_ cuja resposta aos pedidos implica acesso à coleção
de utilizadores e _workspaces_. Em **UsersController** ficam _endpoints_ cuja
resposta aos pedidos implica apenas acesso à coleção de utilizadores. Por
último, em **WorkspacesController** ficam os métodos que implementam métodos
que apenas necessitam de aceder à coleção de _workspaces_ para que seja
possível produzir uma resposta.

A documentação gerada torna claros os _endpoints_ disponíveis em cada um dos
controladores, conforme se pode conferir aqui:

![_Endpoints_ disponíveis em **MainController**](figures/api_doc_main.png){ #fig:api_main }

![_Endpoints_ disponíveis em **UsersController**](figures/api_doc_users.png){ #fig:api_users }

![_Endpoints_ disponíveis em **WorkspacesController**](figures/api_doc_workspaces.png){ #fig:api_workspaces }

Verifiquemos agora a maneira como a documentação torna evidentes as
informações necessárias relativamente a um _endpoint_:

![_Endpoint_: **GET** de um _workspace_ específico](figures/get_workspace_request.png){ #fig:api_request }

Torna-se evidente que este _endpoint_ necessita de uma _Path Variable_ que é
o identificador do _workspace_, retorna um objeto _json_ que contém o `id`,
`name` e `image` do _workspace_. Por último, consegue perceber-se que, em
caso de sucesso, é retornado o código de mensagens HTTP 200, que indica o
sucesso.

## Distribuidor de tarefas - Delegator {#sec:python_tech}

O distribuidor de tarefas cujo modelo apresentamos na análise de requisitos
foi implementado como um microserviço em _Python_. É óbvia a necessidade de
uma biblioteca que implemente as operações necessárias de programação inteira
mista. A biblioteca escolhida foi _Google OR Tools_. Após a tentativa de uso
da biblioteca em _Java_ não ter sido bem sucessiva (uma vez que não
conseguimos importá-la com sucesso) optamos por criar o serviço em _Python_,
linguagem em que conseguimos usar a biblioteca. A desacoplação do serviço
também oferece menos sobrecarga no servidor principal, permitindo correr esta
tarefa exigente em termos de recursos paralelamente e até em múltiplas
máquinas diferentes.

Assim, o modelo descrito foi implementado e lançado em
<https://delegatewise-delegator.herokuapp.com/>, tendo acesso à base de
dados da aplicação. Este serviço é corrido todos os domingos (dia em que não
é permitido o escalonamento de tarefas na aplicação) às 12:00, limpando as
tarefas dessa semana e escalonando as da semana seguinte.

## Tecnologias adotadas no _Backoffice_ {#sec:office_tech}

Conforme constava do enunciado deste trabalho prático, a linguagem de
programação escolhida para a implementação deste componente foi **C#**.
Uma vez que temos pouca experiência com esta linguagem e o _Backoffice_ é
um componente relativamente simples da nossa aplicação esta constitui uma
excelente oportunidade para entrarmos contacto e aprendermos ferramentas
_Microsoft_ que serão extremamente úteis no futuro.

Aliada a **C#** utilizamos uma _framework_ denominada **ASP.NET** que simplifica
imenso a criação de páginas _web_ (utilizando a tecnologia _Razor_ que torna
possível a utilização de código **C#** em páginas _HTML_). A framework
utilizada simplifica, ainda, a criação, utilização e manutençaõ de bases de
dados com a _Entity Framework_ que é uma _framework de object-relational
mapping_.

Era necessário estabelecer uma base de dados para a qual o _Backend_ escrevesse
logs para o que o _Backoffice_ conseguisse obtê-los. Para esta base de dados
escolhemos utilizador _MySQL_ por ser a ferramenta com a qual a equipa de
desenvolvimento do _Backoffice_ estava mais confortável. Este componente
aplicacional, por sua vez, precisa também de armazenar informações específicas
pelo que tem na base de dados um tabela chamada **User** onde guarda
informações relativas aos seus usuários. Se um utilizador do _Backoffice_ for
administrador isto dá-lhe a capacidade de convidar outros usuários para
criarem conta neste componente.

Um usuário que é administrador pode convidar um outro (sendo que é adicionada
uma entrada na tabela _Invitations_ da base de dados). Aquando da realização
de um convite é criada uma _Hash_ que é guardada, juntamente com a data
limite da sua utilização:

| Invitations   |          |
| ------------- | -------- |
| **Id**        | int      |
| Hash          | longtext |
| LimitDateTime | datetime |

: Entrada da tabela _Invitations_ {#tbl:db_invitations}

Aquando de um convite o _Backoffice_ percorre a tabela e remove todos os
convites que estiverem já expirados. Após esta ação, vai verificar a validade
do convite (isto é, ver se existe na tabela alguma entrada para aquela
_Hash_). No caso de a confirmação ser bem sucedida é apresentada, ao futuro
utilizador, uma página de inscriçaão, caso contrário é informado que aquele
_link_ já não está disponível ou é inválido. Uma conta criada através deste
método fica rotulada como um utilizador normal. Para se tornarem
administradores estes novos usuários têm de esperar que um dos
administradores lhes atribuía este rótulo.

A comunicação entre _Backoffice_ e _Backend_ é concretizada através da tabela
_LogsBackend_.

| Logs     |          |
| -------- | -------- |
| **Id**   | INT      |
| UserId   | TEXT     |
| Type     | int      |
| DateTime | datetime |

: Entrada da tabela _Logs_ {#tbl:db_logs}

A cada conexão é executada uma operação de _parsing_ dos dados constantes da
tabela _LogsBackend_ que transforma cada entrada de nível estatístico numa
entrada da tabela _Log_.

Por fim, é necessário implementar a visualização dos dados obtidos de uma
forma intuitiva e facilmente percetível para o utilizador. Para visualizar os
dados constantes da tabela _Log_ utilizamos a libraria _canvasJS_ que é uma
_API_ em **Javascript** que permite criar gráficos a partir de dados em
formatos específicos.

![Exemplo de gráfico no _Backoffice_](figures/logs_image.png){ height=300px #fig:logs }

Deste modo, o utilizador pode escolher qual o tipo de ações cujas
estatísticas pretende consultar de entre um conjunto especificado (nos _Use
Cases_).

Por fim a aplicação foi lançada no _Heroku_ de modo a poder ser consultada
remotamente.

