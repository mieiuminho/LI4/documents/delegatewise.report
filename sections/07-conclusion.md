# Conclusão {#sec:conclusion}

Desde o início deste projeto que estamos bastante satisfeitos com a nossa
ideia. Como é óbvio, no momento em que escolhemos o tema do projeto não
estávamos totalmente cientes da carga de trabalho que este iria representar e
as dificuldades técnicas com as quais nos iríamos deparar. No entanto, a
ideia que tivemos acabou por representar um bom equilíbrio entre a utilidade
que o produto final representa e os desafios e oportunidades de evoluirmos
que nos proporcionou.

Numa primeira fase do projeto (que se designa de _Fundamentação_), realizamos
pesquisa por forma a perceber qual a utilidade da nossa aplicação. Fizemos
uma pequena análise de mercado (não formal) que nos permitiu perceber que o
espaço das aplicações que auxiliam no cumprimento de tarefas mundanas é um
espaço já bastante populado. No entanto, na nossa opinião, muitas das
propostas existentes no mercado servem propósitos semelhantes. Por esta mesma
razão, conseguimos vislumbrar uma quota de mercado considerável para a nossa
proposta uma vez que esta apresenta um fator diferenciador. A nossa aplicação
retira do utilizador a responsabilidade da distribuição de tarefas por
pessoas que as devem realizar.

Posteriormente, desenhamos uma maqueta conceptual e de interface do sistema.
Tentamos iterar a maqueta da interface por forma a obter uma interface
agradável para o utilizador, uma vez que é sabido que o primeiro contacto
do utilizador com a aplicação é extremamente importante, e uma boa experiência
com uma aplicação torna mais provável a permanência do utilizador.

Para que nos conseguíssemos organizar e otimizar o tempo dispendido no
desenvolvimento da aplicação desenvolvemos um diagrama de Gantt. Este diagrama
visava definir intervalos temporais para funcionalidades que sabíamos, à
partida, que teriam de ser implementadas. O aparecimento de necessidades não
previstas inicialmente provocou um ligeiro desvio em relação ao diagrama. Nada
disto invalida, no entanto, o valor fornecido pela tentativa de organização do
tempo.

Assim, no final desta primeira fase tínhamos uma ideia daquilo que eram as
ações que pretendíamos implementar na aplicação, assim como a interface com o
utilizador que pretendíamos construir.

Seguidamente desenvolvemos um modelo de domínio que visava ajudar a perceber
quais os dados que teríamos de guardar de modo a tornar exequível o projeto.
Após a concepção do modelo de domínio e diagrama de classes tornaram-se
evidentes as classes de dados a implementar, uma vez que ambos estes diagramas
eram claros e concisos.

Posteriormente seguiu-se uma fase de estreita comunicação entre as três equipas
de desenvolvimento que estabelecemos (_Frontend_, _Backend_ e _Backoffice_),
uma vez que estes são interdependentes. Estamos bastantes contentes pelo
maneira como comunicamos durante este trabalho, quer através da boa
documentação de todos os _Issues_ e _Merge Requests_ quer pela documentação
automática que utilizamos em alguns componentes (como, por exemplo, no
_Backend_).

Em suma, estamos bastante satisfeitos com o resultado final e aquilo que é o
produto final. No entanto, predomina no grupo a ideia de que o projeto pode ser
melhorado e por isso seguiremos com o desenvolvimento desta aplicação para que
possamos implementar ainda mais funcionalidades e torná-la mais polida,
implementando algumas das sugestões na @sec:discussao.

