# Discussão {#sec:discussao}

Nesta secção faremos uma avaliação comparativa entre a aplicação que nos
propusemos a construir aquando da modelação inicial e recolha de
requisitos.

Após a finalização do trabalho conseguimos constatar que cumprimos todos
os requisitos enumerados inicialmente e acabamos por adicionar
funcionalidades extra cuja utilidade fomos percebendo ao longo do
desenvolvimento do trabalho.

Relembremos os requisitos funcionais enumerados na fase de recolha:

- Inscrição de novos usuários
- Consulta de perfil de usuário
- Alteração de campos do perfil do usuário
- Inscrição de um usuário num determinado _workspace_
- Criação de um _workspace_
- Alteração de campos do _workspace_
- Associar _skills_ a um usuário
- Associar _skills_ a um _workspace_
- Criação de tarefas (no seio de um _workspace_)
- Remoção de tarefas (no seio de um _workspace_)
- Alteração do estado de uma tarefa (marcar como completa, desmarcar...)
- Reportar utilizador num _workspace_ por incumprimento de tarefa

Durante o desenvolvimento da aplicação reparamos, por exemplo, que seria
essencial um mecanismo para que os utilizadores conseguissem recuperar
as credenciais de acesso à sua conta no caso de se esquecerem das mesmas.
Assim desenvolvemos uma opção na aplicação que permite que o utilizador
introduza o seu email e receba no mesmo um link que lhe permite repor
as suas credenciais de acesso.

Existem, no entanto, componentes da nossa aplicação que gostaríamos de ver
mais desenvolvidas, o que não aconteceu devido a restrições temporais.
Gostaríamos de aumentar o leque de opções de consulta disponíveis no
_Backoffice_ (o que implica melhoras quer nos logs produzidos pelo _Backend_
quer um aumento no conjunto de comandos de _Log_ suscetíveis de serem
interpretados pelo _Backoffice_).

Achamos que acrescentaria imenso valor a existência de uma aplicação nativa
_mobile_ para diminuir a fricção no acesso à aplicação através de dispositivos
móveis. Com esta medida esperamos aumentar significativamente o número de
utilizadores, uma vez que os dispositivos móveis representam uma quota enorme
de mercado no presente.

Por último, consideramos que seria positivo criar um logo mais profissional
e personalizado, uma vez que o temos neste momento é gerado automaticamente e,
por isso, não necessariamente aquele que mais se identifica com o propósito
da aplicação desenvolvida.

