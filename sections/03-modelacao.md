# Modelação {#sec:modelation}

## Modelo de domínio {#sec:dominio}

![Modelo de domínio](figures/modelo_dominio.jpg){ #fig:dominio }

## Diagrama de Use Cases {#sec:cases}

![Diagrama de _Use Cases_](figures/use_cases.jpg){ #fig:cases }

## Distribuição de Tarefas {#sec:task_alg}

Ambicionamos, com a nossa aplicação, simplificar a atribuição de tarefas num
contexto de partilha de casa, sendo que se torna necessário desenvolver uma
estratégia que permita distribuir as tarefas que constam dum determinado
_workspace_. Para suprir essa necessidade optamos por desenvolver um modelo
de programação inteira mista que passamos a explicar.

Para representar a distribuição das tarefas são utilizadas variáveis
binárias com o seguinte formato:

- $Xust = 1$ significa que a tarefa **t** foi atribuída ao utilizador **u**.
- $Xust = 0$ significa que a tarefa **t** não foi atribuída ao utilizador **u**.

De notar que o **s** presente nos nomes das variáveis serve apenas como
separador, a sua presença facilita a extração de **u** e **t** da variável em
fases posteriores. **u** e **t** são identificadores hexadecimais, pelo que
nunca incluirão a letra **s**, evitando assim possíveis conflitos.

Já que cada tarefa apenas pode ser atribuída a 1 utilizador, a seguinte
restrição é necessária:

$$Xu1st + Xu2st + … + Xunst <= 1$$

A restrição anterior significa que a tarefa **t** é atribuída a, no máximo, 1
dos utilizadores (u1, u2, …, un).

Sabendo que tanto as tarefas como os utilizadores podem ter _skills_ associadas
e que tarefas de determinada _skill_ apenas podem ser atribuídas a utilizadores
que possuam essa mesma _skill_, seguem as seguintes restrições:

- Para uma tarefa t com uma _skill_ associada:

${u1,u2,u3}$ -> Conjunto de utilizadores que possuem a _skill_.

${u4,u5,u6}$ -> Conjunto de utilizadores que não possuem a _skill_.

$Xu1st + Xu2st + Xu3st = 1$ -> A tarefa é atribuída a 1 e só 1 dos
utilizadores que possuem a _skill_ necessária.

$Xu4st + Xu5st + Xu6st = 0$ -> A tarefa não é atribuída a nenhum utilizador
que não possua a _skill_ necessária.

As restrições já abordadas seriam suficientes para garantir que as tarefas
seriam distribuídas. Não são, no entanto, suficientes para os efeitos do
projeto já que a distribuição deverá ser feita de modo a balancear o mais
possível a carga de trabalho de cada utilizador (_workload_).

Assim sendo, serão necessárias mais restrições e a definição de uma função
objetivo de modo a implementar este critério de seleção. Primeiro, notemos
que o _workload_ de um qualquer utilizador **u**, depois da atribuição das
tarefas, pode ser representado pela seguinte expressão[^1]:

$$Xust1 + Xust2 + … + Xustn + w$$

[^1]: $w$ é o valor do _workload_ acumulado do utilizador até ao momento
imediatamente anterior à distribuição das tarefas.

Fica claro que o balanceamento dos _workloads_ passa por minimizar a
diferença entre o valor máximo e mínimo destas expressões. Chamando MAX e MIN
aos valores máximos e mínimos, respetivamente, podemos definir a função
objetivo do seguinte modo:

$$min~z: MAX - MIN$$

O cálculo de MAX e MIN torna-se algo complexo devido às limitações dos
modelos de programação, a principal delas sendo o facto que o cálculo de um
mínimo de 2 expressões/variáveis requer 2 restrições enquanto que o cálculo
de um máximo de 2 expressões/variáveis requer 4 restrições. Estes cálculos só
podem ser feitos entre 2 expressões/variáveis, o que contribui para a
complexidade.

Assim, o cálculo dos valores máximos e mínimos terá de ser feito de forma
recursiva recorrendo ao uso de MAX e MIN intermédios, comparando duas
expressões até que todas as expressões do _workload_ de utilizadores sejam
comparadas.

Este processo garante que o MAX e MIN utilizados na função objetivo são, de
facto, os valores máximos e mínimos que as expressões de _workload_ dos
utilizadores tomam. Como a função objetivo garante a minimização da diferença
entre MAX e MIN, garantimos assim que a distribuição é a mais balanceada
possível, sem nunca deixar de considerar restrições relativas às _skills_,
entre outras. Por último, será importante notar que em casos específicos
algumas tarefas poderão não ser atribuídas e nenhum utilizador, no caso de
nenhum ter a skill necessária por exemplo, sem que isso altere o normal
funcionamento do modelo. Cabe ao utilizador verificar estes detalhes.

## Modelação da Interface da Aplicação {#sec:modelation_app}

Antes de começarmos a desenvolver o frontend optamos por desenvolver um
modelo da _interface_ e funcionalidades que pretendíamos para a aplicação,
criamos portanto os seguintes _mockups_:

![Landing Page](figures/mockups/land.png){ height=350px #fig:mockups_land }

![Menu Inicial](figures/mockups/menu.png){ height=400px #fig:mockups_menu }

![Tarefas da semana](figures/mockups/tasks.png){ height=400px #fig:mockups_tasks }

![Workspaces do utilizador](figures/mockups/workspaces.png){ height=400px #fig:mockups_workspaces }

![Página de um workspace](figures/mockups/workspace.png){ height=400px #fig:mockups_workspace }

