# Arquitetura da Aplicação {#sec:architecture}

![Interações entre componentes](figures/diagrams/modulos.png){ height=350px #fig:modulos }

## WebApp {#sec:webapp}

Esta componente da aplicação deve ser capaz de oferecer ao utilizador uma forma
fácil e intuitiva de compreender e utilizar as funcionalidades previstas
anteriormente, para isso deve conhecer e ser capaz de comunicar com a _API_
definida pelo _Backend_ e fornecer ao utilizador um conjunto ferramentas para
que este seja capaz de requisitar, visualizar, enviar e modificar a informação
que pretenda, como pretendemos que a aplicação seja _Single Page_ esta terá de
ser _Statefull_, isto é, ser capaz de manter um estado e atuar de maneira
diferente de acordo com a interação do utilizador.

## REST API {#sec:api}

Esta componente da aplicação deve suportar todas as operações de interação com
a base de dados por parte do _Frontend_. Deve suportar ainda operações de
comunicação com _API's_ remotas (como, por exemplo, no _upload_ das imagens de
perfil de usuário) para um _host_ remoto e o envio de _emails_ com _tokens_
temporários necessários à recuperação da conta, em caso de o utilizador se ter
esquecido das credenciais de login na aplicação.

Com base nesta análise emergem três tipos de requisitos principais que esta
componente da aplicação deve assegurar:

- Implementação do modelo de dados.
- Comunicação com a base de dados.
- Comunicação com _API's_ remotas.

Como consequência de ser o _Backend_ a assegurar o funcionamento das operações
mais básicas da aplicação (acessos às bases de dados, por exemplo) terá de
existir um mecanismo que torne informação quantitativa relativa a pedidos
feitos ao _Backend_ disponível para uso estatístico no _Backoffice_.

Será necessária também a implementação de uma base de dados que suporte a
informação aplicacional (perfis de usuários e perfis de _workspaces_).

Seria de valorizar uma ferramenta que simplificasse a interação com a base de
dados (por exemplo: implementar comando próprios da tecnologia de bases de
dados como métodos _Java_ para que possamos abstrair da tecnologia usada e
tornar o código escrito reutilizável e totalmente independente da implementação
de bases de dados escolhida).

Algumas das operações acima enumeradas levantam algumas questões de
implementação relevantes, como, por exemplo: que mecanismo usaremos para
validar as credenciais de login. Temos de utilizar (ou desenvolver) algum
mecanismo que nos permita comunicar à entidade que realiza pedidos se o login
foi ou não bem sucedido. Essa maneira de comunicação tem ainda de lhe fornecer
uma maneira de autenticar pedidos futuros, uma vez que estamos a falar de uma
_REST API_ e, por isso, _Stateless_ querendo isto dizer que a _API_ não guarda
informações relativas ao estado e não sabe, por isso, se quem está a realizar o
pedido se autenticou previamente.

Por questões de segurança a maior parte dos _endpoints_ da _API_ devem requerer
autenticação uma vez que fornecem acesso a informação sensível. Existem, no
entanto, alguns _endpoints_ que devem ser abertos, por definição. O _endpoint_
que permite fazer um pedido de login deve ser aberto, para que todos os
possíveis utilizadores se possam autenticar. O _endpoint_ que permite registar
uma conta de usuário deve também ser aberto para permitir o registo de novos
usuários.

## Backoffice {#sec:backoffice}

Em termos arquiteturais este componente necessita de estabelecer comunicação
com o _Backend_ (embora de forma indireta, através da base de dados de _Logs_
já mencionada). Deste modo, o _Backoffice_ apenas necessita de estabelecer
conexão com essa base de dados relacional para garantir pleno funcionamento,
desde que esta seja continuamente atualizada pelo _Backend_.

