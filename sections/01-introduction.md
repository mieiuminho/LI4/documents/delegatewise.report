# Introdução {#sec:introduction}

O título do nosso projeto é: **Delegatewise - Task Sharing Simplified**.

A ideia desta aplicação surgiu na medida em que sentimos necessidade de
uma aplicação que implementasse a distribuição justa de tarefas num ambiente
cooperativo, por exemplo: numa casa onde habitam diversas pessoas.

A necessidade da aplicação justifica-se na medida em que existem já
diversas aplicações para a distribuição de tarefas em grupos organizados,
como, por exemplo, o Trello. O software por nós desenvolvido diverge dos
existentes na medida em que propomos a distribuição automática de tarefas
mundanas com as quais os nossos usuários não queiram perder tempo a
delegar.

A aplicação apresentada deve suportar a introdução de tarefas por parte
de uma entidade que controla um conjunto de utilizadores (pode ser, por
exemplo, um dos habitantes de um espaço partilhado). Uma vez introduzidas
as tarefas estas devem ser divididas pela aplicação entre os
participantes da "equipa" de forma equitativa.

Assim, esta aplicação pretende implementar a distribuição justa de tarefas
no seio de uma equipa eliminando quer o trabalho quer o tempo de distribuir
as tarefas pelos diversos elementos disponíveis na equipa.

Este relatório visa explicar como é que a solução foi construída, os
problemas e desafios encontrados e a abertura a trabalho futuro. O documento
está estruturado do seguinte modo. Na @sec:requirements apresenta-se a
especificação do problema e os requisitos essenciais da solução. A
arquitetura da solução é apresentada na @sec:architecture . A
@sec:implementation explica a solução. Por fim, a @sec:conclusion discute
trabalho futuro.

