# Levantamento de Requisitos {#sec:requirements}

A fase de especificação do projeto consistiu no levantamento dos requisitos
mínimos para criar um produto viável. Este requisitos dividem-se em
funcionais e não funcionais. O conjunto de requisitos serão traduzidas para o
conjuntos de funcionalidades mínimas do sistema.

O _delegatewise_ consiste numa aplicação de distribuição de tarefas entre
grupos de pessoas e como tal necessita de existir a noção de grupo assim como
a noção de tarefa. A atribuição de tarefas será feita a utilizadores que
estejam no grupo em que a tarefa esteja associada.

## Requisitos Funcionais {#sec:functional}

Os requisitos funcionais consistem nas funcionalidades que a aplicação deverá
suportar e podem ser resumidas na lista seguinte:

- Inscrição de novos usuários
- Consulta de perfil de usuário
- Alteração de campos do perfil do usuário
- Convite de um usuário num determinado _workspace_
- Criação de um _workspace_
- Alteração de campos do _workspace_
- Associar _skills_ a um usuário
- Associar _skills_ a um _workspace_
- Criação de tarefas (no seio de um _workspace_)
- Remoção de tarefas (no seio de um _workspace_)
- Alteração do estado de uma tarefa (marcar como completa, desmarcar...)
- Reportar utilizador num _workspace_ por incumprimento de tarefas

De maneira a responder aos requisitos funcionais da aplicação terá de ser
implementada uma solução que seja capaz de efetuar, pelo menos, esse conjunto
de acções.

Sabemos ainda que este trabalho deve ainda contar com um _Backoffice_ com recurso
a ferramentas _Microsoft_ (esta informação pode ser obtida os slides de
apresentação da cadeira). Desta forma, esta componente da aplicação deve suportar
a gestão e consulta de estatísticas relativas ao uso da aplicação, ações essas
que podem apenas ser realizadas por utilizadores autorizados. Assim sendo, é
possível estabelecer alguns requisitos básicos para esta secção da aplicação:

- Consultar o número de _LogIn's_ num período de tempo especificado
- Consultar o número de registo de conta num período de tempo especificado
- Consultar o número de contas removidas num período de tempo especificado
- Consultar o número de _workspaces_ criados num período de tempo especificado
- Consultar o número de _workspaces_ removidos num período de tempo especificado
- Consultar o número de tarefas num período de tempo especificado
- Consultar o número de utilizadores reportados por incumprimento de tarefas
  num determinado período tempo especificado
- Alteração da informação da conta
- Convite para outros usuários

Os requisitos para o _Backoffice_ foram definidos tendo em conta a manutenção
da privacidade dos utilizadores e por isso preconizam apenas informação
estatística (por exemplo: sabemos quantas contas são criadas num dia mas não
sabemos que contas).

## Requisitos Não Funcionais {#sec:non_functional}

A aplicação deverá de ser de fácil interação e todas as acções possíveis
deverão estar bem organizadas numa interface gráfica intuitiva. Esta
aplicação deverá estar disponível no maior número de dispositivos possíveis
assim como ser possível de interagir por múltiplos utilizadores
simultaneamente.

Este tipo de requisitos têm um papel muito nas decisões tecnológicas e
arquiteturais das secções futuras.

